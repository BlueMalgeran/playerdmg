# PlayerDMG
A simple plugin that shows your damage to the players.
No config file or other thirdsided files are required to make the plugin work!
# Example:
![alt text](https://i.imgur.com/F6f3q3o.png)
# Note:
When admin / player / VIP is connecting to the server it'll show it up in chat, so welcome messages are included in this plugin.
![alt text](https://i.imgur.com/HRLBKE4.png)
# You can delete the lines 17 - 32 if you don't want the "welcome messages".
